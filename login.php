<?php
	require_once "coneccion.php";
	session_start();
	date_default_timezone_set('America/Chihuahua');
	$hoy = date('Y-m-d G:ia');
	

	if( isset($_POST['login']) )
	{
		if( (trim($_POST['name']) !== '') && (trim($_POST['pwd']) !== '') ){
			
			$name = trim($_POST['name']);
			$pwd = trim($_POST['pwd']);
		
			    $qry="SELECT * FROM usuarios WHERE nombre=:name LIMIT 1";
				$stmt = $link->prepare($qry);
				$stmt->execute(array(':name' => $name));
				$res = $stmt->fetch(PDO::FETCH_ASSOC);
				
				
				
	
				$contrasena = ($res['contrasena']);
				
				//obtener id de usuario
				
				//con este id de usuario hacer una búsqueda (query) para obtener los intentos fallidos de acceso.
				
				if (password_verify($pwd, $contrasena)){
					$_SESSION['success'] = "¡Autenticacion exitosa";
					$intentos = 0;
					$qry="UPDATE usuarios SET bandera=0 WHERE nombre=:name LIMIT 1";
						$stmt = $link->prepare($qry);
						$stmt->execute(array(':name' => $name));
					
				}
				else{
					$_SESSION['error'] = "¡Autenticacion fallo!";
					
					$intentos = $_SESSION['intentos'];
					if( $intentos<3 )
					{
						$intentos++;
						
					}
					else{
					    $_SESSION['error'] = "¡Cuenta bloqueada, por mas de 3 intentos!";
						$qry="UPDATE usuarios SET bandera=1 WHERE nombre=:name LIMIT 1";
						$stmt = $link->prepare($qry);
						$stmt->execute(array(':name' => $name));
						
					}
					
				}
				
				
				$_SESSION['intentos'] = $intentos;
					
		}	
		
					
		header("location:login.php");
		return;	
		
	}
	
	
?>

<!DOCTYPE html>
<html>
	<head>
	</head>
	<body>
		<h1>Login</h1>
		<?php	
			if( isset($_SESSION['success']) ){
				echo '<p style="color:green;">'.htmlentities(trim($_SESSION['success'])).'</p>';
				unset($_SESSION['success']);
			}
			if( isset($_SESSION['error']) ){
				echo '<p style="color:red;">'.htmlentities($_SESSION['error']).'</p>';	
				echo '<p style="color:red;">Intentos fallidos: '.htmlentities($_SESSION['intentos']).'</p>';	
				unset($_SESSION['error']);
			}
			if( isset($_SESSION['login']) ){
				echo '<p style="color:blue;">'.htmlentities($_SESSION['login']).'</p>';
				unset($_SESSION['login']);
			}			
		?>
		<form method="post">
			<span>Usuario:</span><input type="text" name="name" placeholder="">
			<br>
			<span>Contrasena:</span><input type="password" name="pwd" placeholder="">
			<br>
			<br>
			<input type="submit" name="login" value="Login">
			</form>
			<br>
			<a href="signup.php"><button>Sign up</button></a>

	</body>
</html>