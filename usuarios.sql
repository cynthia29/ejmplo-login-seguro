-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-07-2020 a las 21:37:09
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `comunitec32k`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsuario` smallint(6) NOT NULL,
  `nombre` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `contrasena` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `fechaRegistro` datetime NOT NULL DEFAULT current_timestamp(),
  `bandera` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsuario`, `nombre`, `correo`, `contrasena`, `fechaRegistro`, `bandera`) VALUES
(1, 'cynthia', 'cynthia@gmail.com', '123', '2020-06-26 10:27:53', b'0'),
(21, 'blas', 'blas@gmail.com', 'b4', '2020-06-27 11:11:03', b'0'),
(22, 'blasesito', 'blas4@gmail.com', '$argon2id$v=19$m=65536,t=4,p=1$cGxiOWxldnNmL2dHLi94MA$IzBdLXINrRcWmFnvVm/IdIfTHCBJ7kAELVaCBnLeE7M', '2020-06-27 11:16:22', b'0'),
(40, 'mayo', 'mayo@gmail.com', '$argon2id$v=19$m=65536,t=4,p=1$NDBIVjQzaUo4bDF1dmNGMw$RqxjEuKvdR0/CMssklo2P8SxdjmcCQJJTplA+YKJuLU', '2020-07-03 16:19:37', b'0'),
(41, 'julio', 'julio@gmail.com', '$argon2id$v=19$m=65536,t=4,p=1$V2dpb0JTLllnSVNSdmU4MQ$AmD7FaQK8BlKKS1vsFyN6HQaSNn4GJ9mQw73HSR30Lk', '2020-07-03 16:21:25', b'1'),
(42, 'diciembre', 'diciembre@gmail.com', '$argon2id$v=19$m=65536,t=4,p=1$anZPLkNtaDBFZjJVQ1Myaw$bQtfnyYCX2y7mi0ZQbRYfHVEs7k8ov7mBKt8Oeh/PGo', '2020-07-03 16:22:05', b'1'),
(43, 'netflix', 'net@gmail.com', '$argon2id$v=19$m=65536,t=4,p=1$Tk14R2M3ODEyNTlSZlNtTA$DP/ofGpzxnUqmtk5lwpvRtwKFFiLevAdrYat+GwumAU', '2020-07-03 16:25:12', b'1'),
(44, 'juan', 'juan@gmail.com', '$argon2id$v=19$m=65536,t=4,p=1$VEs3OGdrZlQ4MzlCT05EaA$xtaqbWk8BOgsT6PBJxk1GZQWv874MplvmLP62jE2fDs', '2020-07-03 16:28:09', b'1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsuario` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
